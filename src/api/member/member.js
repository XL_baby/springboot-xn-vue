import request from '@/utils/request'

export function memberPage(query) {
	return request({
		url: "/member/page",
		method: "get",
		params: query
	});
}

export function memberSave(obj) {
	return request({
		url: "/member/submit",
		method: "post",
		data: obj
	});
}

export function memberRemove(obj) {
	return request({
		url: "/member/remove",
		method: "post",
		data: obj
	});
}
